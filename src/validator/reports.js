import { validate } from './core'


export const findUserByEmailValidator = (req) => {
    const validators = [
        {
            field: 'email',
            predicate: (param) => param.match(/[a-zA-z._-]+/),
            predicateErrorMsg: 'path parameter field must satisfy [a-zA-z._-]+'
        }
    ];

    return validate(validators, req.params);
}

export const getReportByUserValidator = (req) => {
    const validators = [
        {
            field: 'id',
            predicate: (param) => param.match(/[0-9]+/),
            predicateErrorMsg: 'path parameter field must satisfy [0-9]'
        }
    ];

    return validate(validators, req.params);
}

export const updateReportValidator = (req) => {
    const validators = [
        {
            field: 'reportId',
            predicate: (param) => param.match(/[0-9]+/),
            predicateErrorMsg: 'field reportId must satisfy [0-9]+'
        },
        {
            field: 'statusId',
            predicate: (param) => param.match(/[0-9]+/),
            predicateErrorMsg: 'field statusId must satisfy [0-9]+'
        }
    ];

    return validate(validators, req.body);
}