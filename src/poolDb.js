import mysql from 'mysql';
import dotenv from 'dotenv';

dotenv.config();

const extranetHost = process.env.MYSQL_DB_EXTRANET_HOST;
const extranetUser = process.env.MYSQL_DB_EXTRANET_USER;
const extranetPassword = process.env.MYSQL_DB_EXTRANET_PASSWORD;
const extranetDatabase = process.env.MYSQL_DB_EXTRANET_DATABASE;

const backofficeHost = process.env.MYSQL_DB_BACKOFFICE_HOST;
const backofficeUser = process.env.MYSQL_DB_BACKOFFICE_USER;
const backofficePassword = process.env.MYSQL_DB_BACKOFFICE_PASSWORD;
const backofficeDatabase = process.env.MYSQL_DB_BACKOFFICE_DATABASE;

let dbExtranet = mysql.createConnection({
    host     : extranetHost,
    user     : extranetUser,
    password : extranetPassword,
    database : extranetDatabase
});

let dbBackoffice = mysql.createConnection({
    host     : backofficeHost,
    user     : backofficeUser,
    password : backofficePassword,
    database : backofficeDatabase
});

dbExtranet.connect();
dbBackoffice.connect();

export default {
    "dbExtranet": dbExtranet,
    "dbBackoffice": dbBackoffice
}


