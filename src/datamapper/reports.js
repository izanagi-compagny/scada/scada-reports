import {findUserByEmailRepo, getAllUsers} from "../repository/users";
import { getReportByUserRepo, updateReportRepo } from "../repository/reports";
import { run } from "./core";

export const getUsers = (res) => {
    getAllUsers(res);
};

export const findUserByEmail = (res, email) => {
    findUserByEmailRepo(res, email);
};

export const getReportByUser = (res, userId) => {
    const datamapper = [
        {
            source: 'idReport',
            target: 'report_id'
        },
        {
            source: 'idStatus',
            target: 'status_id'
        },
        {
            source: 'idType',
            target: 'type_id',
            transform: (id) => 'transaction'
        },
        {
            source: 'idUser',
            target: 'user_id'
        }
    ];

    getReportByUserRepo(res, userId, run(datamapper))
}

export const updateReport = (res, req) => {
    const statusId = req.body.statusId;
    const reportId = req.body.reportId;

    updateReportRepo(res, statusId, reportId);
}





