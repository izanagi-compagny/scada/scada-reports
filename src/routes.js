import express from 'express'
import {getUsersAction, getReportByUserAction, updateReportStatus, findUserByEmailAction} from "./controller/reports";

let router = express.Router();

router.get('/', (req,res) => {
   res.send('coucou');
});

router.get('/users', getUsersAction);
router.get('/users/:email', findUserByEmailAction);

router.get('/:id', getReportByUserAction);

router.put('/', updateReportStatus);

export default router;
