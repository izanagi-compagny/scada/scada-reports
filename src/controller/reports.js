import poolDb from '../poolDb';
import mysql from 'mysql';
import {getUsers, getReportByUser, updateReport, findUserByEmail} from "../datamapper/reports";
import {findUserByEmailValidator, getReportByUserValidator, updateReportValidator} from "../validator/reports";

export const getUsersAction = (req, res) => {
        getUsers(res);
};

export const findUserByEmailAction = (req, res) => {
  const errors = findUserByEmailValidator(req);

    if (errors.length > 0)
        badRequest(res, errors)
    else
        findUserByEmail(res, req.params.email);
};

export const getReportByUserAction = (req, res) => {

    const errors = getReportByUserValidator(req);

    if (errors.length > 0)
        badRequest(res, errors)
    else
        getReportByUser(res, req.params.id)
}

export const updateReportStatus = (req, res) => {
    const errors = updateReportValidator(req);

    if (errors.length > 0)
        badRequest(res, errors)
    else
        updateReport(res, req)
}


const badRequest = (res, errors) => {
    res.status(400);
    res.json({
        errors: errors
    });
}