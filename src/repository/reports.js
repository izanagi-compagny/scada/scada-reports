import poolDb from "../poolDb";
import mysql from "mysql";

const db = poolDb.dbBackoffice;

export const getReportByUserRepo = (res, userId, callback) => {

    let sql = 'SELECT * from reports inner join userReports ON userReports.idReport = reports.id WHERE idUser = ?'
    let params = [userId];

    db.query(mysql.format(sql, params), function (error, results, fields) {
        res.json({
            data: callback(results)
        })
    });
}

export const updateReportRepo = (res, statusId, reportId) => {
    let sql = 'UPDATE reports SET idStatus = ? WHERE id = ?'
    let params = [statusId, reportId];

    db.query(mysql.format(sql, params), function (error, results, fields) {
        res.json({
            data: "Update successful"
        })
    });
}